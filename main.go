package main

import (
	"context"
	"flag"
	"os"

	corev1 "k8s.io/api/core/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/config"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"
)

func init() {
	log.SetLogger(zap.New())
}

func main() {
	entryLog := log.Log.WithName("node-capacity-update")
	flag.Parse()

	clientConfig := config.GetConfigOrDie()
	k8sClient, err := client.New(clientConfig, client.Options{})
	if err != nil {
		entryLog.Error(err, "unable to create kubernetes client")
		os.Exit(1)
	}

	nodeList := &corev1.NodeList{}
	err = k8sClient.List(context.Background(), nodeList)
	if err != nil {
		entryLog.Error(err, "unable to list nodes")
	}

	entryLog.Info("node list", nodeList)
}
